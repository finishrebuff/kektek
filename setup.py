from setuptools import setup

setup(
    name="kektek",
    version="0.1",
    author="Clown World",
    license="MIT",
    packages=["kektek"],
    install_requires=[
        "bs4",
        "click",
        "requests",
        "ffmpy",
        "srt",
        "python-dateutil",
    ],
    zip_safe=False,
    entry_points="""
        [console_scripts]
        kektek=kektek.__main__:kektek
      """,
)
