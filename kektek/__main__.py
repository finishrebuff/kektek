#!/bin/env python

# pylint: disable=missing-function-docstring,missing-module-docstring,invalid-name,bad-continuation

import logging

import click

import kektek.crawler as _crawler
import kektek.scanner as _scanner
import kektek.clipper as _clipper

logging.basicConfig()
logging.root.setLevel("INFO")
logger = logging.getLogger(__name__)


@click.group()
def kektek():
    """The Swiss army knife of counter porkabanda

    Est. 1834
    """


@kektek.command()
@click.option(
    "--folder",
    default="srts",
    type=click.Path(),
    help="Specifies the destination folder",
)
@click.option(
    "--resume",
    help="Do not redownload srt files if they already exist. ",
    is_flag=True,
    default=True,
)
def crawl(folder, resume):
    """ Download srt files from www.addic7ed.com"""
    _crawler.run(folder, resume)


@kektek.command()
@click.argument("srt-files", type=click.File(), nargs=-1, required=True)
@click.option("--keywords-file", type=click.File(), required=True)
@click.option("--output-path", default="matches", type=click.Path())
def scan(srt_files, keywords_file, output_path):
    """Scan a bunch of srt files for a list of given key words"""
    _scanner.run(srt_files, keywords_file, output_path)


@kektek.command()
@click.option("--matches-file", type=click.File(), help="Srt file path")
@click.option("--video-file", type=str, help="Video file path.srt and .mp4/.mkv")
@click.option("--output-path")
def clip(matches_file, video_file, output_path):
    """ Create clips from a mkv and srt file"""
    _clipper.run(matches_file, video_file, output_path)


if __name__ == "__main__":
    kektek()
