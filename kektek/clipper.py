import datetime
import logging
import json
import dateutil.parser as dtparser
import ffmpy
import srt


logger = logging.getLogger(__name__)


def create_clips(
    matches,
    video_filepath,
    output_name,
    audio=False,
    subtitles=True,
    interval_buffer=datetime.timedelta(seconds=15),
    clip_length=datetime.timedelta(seconds=30),
):
    outputs = []
    for (scene_no, scene) in enumerate(matches):
        scene_output_name = output_name.replace(
            ".webm", "_{}.webm".format(scene_no + 1)
        )
        worker = ffmpy.FFmpeg(
            inputs={video_filepath: None},
            outputs={
                scene_output_name: "-crf 4 -sn -vf scale=640:-1 -c:v libvpx -ss {} -t {} {} {}".format(
                    dtparser.parse(scene["start"]) - interval_buffer,
                    clip_length,
                    (subtitles) * "-vf subtitles='{}'".format(scene["file"]),
                    (not audio) * "-an",
                )
            },
        )
        outputs += [scene_output_name]
        print(worker.cmd)
        worker.run()

    input_dict = {}
    for output in outputs:
        input_dict[output] = None

    filter_complex_string = "{}concat=n={}:v=1:a={}[outv]{} -map '[outv]'".format(
        "".join(
            [
                "[{}:v:0]".format(i) + (audio) * "[{}:a:0]".format(i)
                for i, _ in enumerate(outputs)
            ]
        ),
        len(outputs),
        (audio) * 1,
        (audio) * "[outa] -map'[outa]'",
    )
    worker = ffmpy.FFmpeg(
        inputs=input_dict,
        outputs={
            "output.webm": "-crf 4 -sn -c:v libvpx -filter_complex {}".format(
                filter_complex_string
            )
        },
    )
    worker.run()


def run(matches_file, video_file, output_path):
    matches = [json.loads(match) for match in matches_file]
    create_clips(matches, video_file, output_path)
