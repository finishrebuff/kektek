import os
import re
import json

import srt


def clean_up(text):
    return (
        text.replace("!", "").replace(".", "").replace(",", "").replace("?", "").lower()
    )


def parse_srt(srt_file, keywords):
    matches = []
    for sentence in srt.parse(srt_file):
        text = sentence.content
        matches.extend(
            {
                "file": srt_file.name,
                "sentence": text,
                "keyword": keyword,
                "start": str(sentence.start),
                "end": str(sentence.end),
            }
            for keyword in keywords
            if re.search(rf"(^|\s){keyword}($|\s)", clean_up(text))
        )
    return matches


def run(srt_files, keywords_file, output_path):
    if not os.path.exists(output_path):
        os.mkdir(output_path)

    keywords = {line.lower().rstrip() for line in keywords_file}
    for srt_file in srt_files:
        show_name, episode_id, *_ = srt_file.name.split("-", 2)
        show_name = show_name.rpartition("/")[2]
        output_filename = show_name[:-1] + episode_id + "matches.jsonl"
        detected_list = parse_srt(srt_file, keywords)
        with open(f"{output_path}/{output_filename}", "w") as output_file:
            for detected in detected_list:
                line = json.dumps(detected)
                output_file.write(line + "\n")
